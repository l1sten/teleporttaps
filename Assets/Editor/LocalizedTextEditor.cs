﻿
using UnityEngine;
using UnityEditor;
using System.IO;

public class LocalizedTextEditor : EditorWindow
{
    public LocalizationData p_LocalizationData;
    [MenuItem("Window/Localized Text Editor")]
    static void Init()
    {
        EditorWindow.GetWindow(typeof(LocalizedTextEditor)).Show();
    }

    private void OnGUI()
    {
        if (p_LocalizationData != null)
        {
            SerializedObject l_serializedObject = new SerializedObject(this);
            SerializedProperty l_serializedProperty = l_serializedObject.FindProperty("p_LocalizationData");
            EditorGUILayout.PropertyField(l_serializedProperty, true);
            l_serializedObject.ApplyModifiedProperties();

            if (GUILayout.Button("Save data"))
            {
                SaveGameData();
            }
        }

        if (GUILayout.Button("Load data"))
        {
            LoadGameData();
        }

        if (GUILayout.Button("Create new data"))
        {
            CreateNewData();
        }
    }

    private void LoadGameData()
    {
        string l_filePath = EditorUtility.OpenFilePanel("Select localization data file", Application.streamingAssetsPath, "json");
        if (!string.IsNullOrEmpty(l_filePath))
        {
            string l_dataJson = File.ReadAllText(l_filePath);

            p_LocalizationData = JsonUtility.FromJson<LocalizationData>(l_dataJson);
        }
    }

    private void SaveGameData()
    {
        string l_filePath = EditorUtility.SaveFilePanel("Save localization data file", Application.streamingAssetsPath, "", "json");

        if (!string.IsNullOrEmpty(l_filePath))
        {
            string l_dataAsJson = JsonUtility.ToJson(p_LocalizationData);
            File.WriteAllText(l_filePath, l_dataAsJson);
        }
    }

    private void CreateNewData()
    {
        p_LocalizationData = new LocalizationData();
    }
	
}