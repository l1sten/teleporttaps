﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocalizedText : MonoBehaviour
{

	public string p_Key = null;
    private Text l_text = null;

    private void Awake()
    {
        l_text = GetComponent<Text>();
    }

    private void Start()
    {
        l_text.text = LocalizationManager.instance.GetLocalizedValue(p_Key);
    }

}
