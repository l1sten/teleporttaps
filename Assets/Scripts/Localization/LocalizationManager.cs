﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class LocalizationManager : MonoBehaviour
{

    public static LocalizationManager instance;

    private Dictionary<string, string> p_localizedText;
    private bool p_isReady = false;
    private string p_missingTextstring = "Localized text not found";

    private void Awake()
    {
        instance = this;
        DontDestroyOnLoad(gameObject);
    }
    
	
	public void LoadLocalizedText(string l_filename)
    {
		p_localizedText = new Dictionary<string, string> ();

        string l_filePath = Path.Combine(Application.streamingAssetsPath, l_filename);

        if (File.Exists(l_filePath))
        {
            string l_dataJSON = File.ReadAllText(l_filePath);

            LocalizationData l_loadedData = JsonUtility.FromJson<LocalizationData>(l_dataJSON);

            for (int i = 0; i < l_loadedData.items.Length; i++)
            {
                p_localizedText.Add(l_loadedData.items[i].key, l_loadedData.items[i].value);
            }
            print("Data loaded, dictionary contains: " + p_localizedText.Count + " entries");
        }
        else
        {
            print("File Exists");
        }

        p_isReady = true;
    }

    public string GetLocalizedValue(string l_key)
    {
        string l_result = p_missingTextstring;
        if (p_localizedText.ContainsKey(l_key))
        {
            l_result = p_localizedText[l_key];
        }

        return l_result;
    }

    public bool p_GetisReady()
    {
        return p_isReady;
    }
}
