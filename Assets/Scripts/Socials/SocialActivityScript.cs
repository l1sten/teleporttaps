﻿using UnityEngine;

public class SocialActivityScript : MonoBehaviour
{
    public static SocialActivityScript instance;

    public string TwitterLink = "https://twitter.com/weekend_games";
    public string AppLink = "market://details?id=com.WG.TeleportTaps";
    public string subject = "";
    public string title = "RacerTP";
    public string message = "Hey there! Looks that amazing game! Link: ";

    private void Awake()
    {
        instance = this;
    }

    public void ShareTwitter()
    {
        Application.OpenURL("https://twitter.com/intent/tweet?text=" + WWW.EscapeURL(message + AppLink) + "&amp;lang=" + WWW.EscapeURL("en"));
    }
    public void OpenOutProfile()
    {
        Application.OpenURL(TwitterLink);
    }

    public void RateUs() {
#if UNITY_ANDROID
        Application.OpenURL(AppLink);;
#endif
    }

    public void Sharing_Image() {
        Application.CaptureScreenshot("Screenshot_game.png");
        SharingImage(Application.persistentDataPath + "/Screenshot_game.png", subject, title, message);
    }
    private void SharingImage(string imageFileName, string subject, string title, string message) {
#if UNITY_ANDROID

        AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
        AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");

        intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
        intentObject.Call<AndroidJavaObject>("setType", "image/*");
        intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), subject);
        intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TITLE"), title);
        intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), message + AppLink);

        AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
        AndroidJavaObject fileObject = new AndroidJavaObject("java.io.File", imageFileName);
        AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("fromFile", fileObject);

        bool fileExist = fileObject.Call<bool>("exists");
        Debug.Log("File exist : " + fileExist);
        // Attach image to intent
        if (fileExist)
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
        AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
        currentActivity.Call("startActivity", intentObject);
#endif
    }
}
