﻿using UnityEngine;
using GoogleMobileAds.Api;
using GooglePlayGames;
public class MyADS : MonoBehaviour {
    InterstitialAd interstitial;

    System.DateTime date = new System.DateTime();
    public void RequestInterstitial()
    { 
        #if UNITY_ANDROID
                string adUnitId = "ca-app-pub-1707788230231812/2350160281";
        #elif UNITY_IPHONE
                string adUnitId = "INSERT_IOS_INTERSTITIAL_AD_UNIT_ID_HERE";
        #else
                string adUnitId = "unexpected_platform";
        #endif

        // Initialize an InterstitialAd.
        interstitial = new InterstitialAd(adUnitId);
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().SetGender(Gender.Unknown).Build();
        // Load the interstitial with the request.
        interstitial.LoadAd(request);
    }
    public void GameOver() {
        if (interstitial.IsLoaded()) {
            interstitial.Show();
            Invoke("Clear", 5f);
        }
    }
    private void Clear() {
        interstitial.Destroy();
    }
}
