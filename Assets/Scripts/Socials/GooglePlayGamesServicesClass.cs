﻿
#if UNITY_ANDROID
using UnityEngine;
using System.Collections;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

public class GooglePlayGamesServicesClass : MonoBehaviour
{

	public string p_leaderBoardName;

    private void Start ()
    {
		PlayGamesPlatform.DebugLogEnabled = true;
		PlayGamesPlatform.Activate ();
        
		p_leaderBoardName = GPGSIds.leaderboard_top_distance;
		LogIn ();
	}

	
	public void LogIn()
    {
		Social.localUser.Authenticate ((bool success) => {
			if (success) {
			} else {
				Debug.Log ("Login failed");
			}
		});
	}

	public void OnShowLeaderBoard()
    {
        PlayGamesPlatform.Instance.ShowLeaderboardUI(p_leaderBoardName);
	}

	public void OnAddScoreToLeaderBoard(float l_score)
    {
		if (Social.localUser.authenticated)
        {
            l_score *= 1000;
			Social.ReportScore ((long)l_score, p_leaderBoardName, (bool success) => {
				if (success) {
					Debug.Log ("Score sended success");
				} else {
					Debug.Log ("Score sended failed");
				}
			});
		}
	}

	public void OnLogout(){
		((PlayGamesPlatform)Social.Active).SignOut ();
	}
}
#endif
