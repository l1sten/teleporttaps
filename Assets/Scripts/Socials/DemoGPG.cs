﻿using UnityEngine;
using System.Collections;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

public class DemoGPG : MonoBehaviour {

	public string _leaderBoard;
    public string Username;
    public string UserID;
    void Start () {
		PlayGamesPlatform.DebugLogEnabled = true;
		PlayGamesPlatform.Activate ();
        
		_leaderBoard = GPGSIds.leaderboard__;
		LogIn ();
	}
	
	public void LogIn(){
		Social.localUser.Authenticate ((bool success) => {
			if (success) {
                Username = Social.localUser.userName;
                UserID = Social.localUser.id;
			} else {
				Debug.Log ("Login failed");
			}
		});
	}

	public void OnShowLeaderBoard(){
        PlayGamesPlatform.Instance.ShowLeaderboardUI(_leaderBoard);
	}

	public void OnAddScoreToLeaderBoard(int score){
		if (Social.localUser.authenticated) {
			Social.ReportScore (score, _leaderBoard, (bool success) => {
				if (success) {
					Debug.Log ("p_score sended success");
				} else {
					Debug.Log ("p_score sended failed");
				}
			});
		}
	}
	public void OnLogout(){
		((PlayGamesPlatform)Social.Active).SignOut ();
	}
}
