﻿#if UNITY_ANDROID
using UnityEngine;
using GoogleMobileAds.Api;
using GooglePlayGames;
using System.Collections;

public class ADSManager : MonoBehaviour
{
    private const int const_playerDied = 2;
    private int p_countDeath = 0;
    private float p_localTimer = 0;
    public static ADSManager instance;

    private InterstitialAd _interstitial;
    private bool p_oneIncrement = false;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
#if !UNITY_EDITOR
        RequestInterstitial();
#endif
    }

    IEnumerator CheckTime()
    {
        while (this.isActiveAndEnabled)
        {
            p_localTimer += Time.deltaTime;
            yield return null;
        }
    }

    private void RequestInterstitial()
    { 
#if UNITY_ANDROID
                string adUnitId = "ca-app-pub-1707788230231812/2350160281";
#elif UNITY_IPHONE
                string adUnitId = "INSERT_IOS_INTERSTITIAL_AD_UNIT_ID_HERE";
#else
                string adUnitId = "unexpected_platform";
#endif

        // Initialize an InterstitialAd.
        _interstitial = new InterstitialAd(adUnitId);
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().SetGender(Gender.Unknown).Build();
        // Load the _interstitial with the request.
        _interstitial.LoadAd(request);
    }

    public void ShowADS()
    {
#if !UNITY_EDITOR
        if (_interstitial.IsLoaded())
        {
            _interstitial.Show();
        }
#endif
    }
    

    private void Clear()
    {
        if (p_localTimer % 300 == 0)
        {
            //Clear ads cash
            _interstitial.Destroy();
            // Request new ads
            RequestInterstitial();
        }
    }

    public void CheckAds()
    {
        p_oneIncrement = true;
        if (p_oneIncrement)
        {
            p_countDeath++;
        }

        if (p_countDeath >= const_playerDied)
        {
            ShowADS();
        }
        p_oneIncrement = true;
    }

    private void OnDestroy()
    {
        instance = null;
    }
}
#endif