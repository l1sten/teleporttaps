﻿using UnityEngine;
public class PlayerController : MonoBehaviour
{
    public static PlayerController instance;
    [SerializeField]
    private LayerMask _msk;
	private GameController _gameController;
    public GameObject _player;


	void Awake(){
        instance = this;
        _gameController = GameController.instance;
	}

    private void OnEnable()
    {
        GameController.e_onclicked += TapScreen;
    }
    private void OnDisable()
    {
        GameController.e_onclicked -= TapScreen;
    }
    public void TapScreen()
    {
        if (_player != null)
        {
#if !UNITY_EDITOR
		if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Began) {
			if (_gameController.p_GameStarted)
			{
				RaycastHit hit;
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

				if (Physics.Raycast(ray, out hit, Mathf.Infinity, _msk))
				{
					if (hit.collider != null)
					{
						Teleport(hit);
					}
				}
			}
		}
#else
            if (Input.GetMouseButtonDown(0))
            {
                if (_gameController.p_GameStarted)
                {
                    RaycastHit hit;
                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                    if (Physics.Raycast(ray, out hit, Mathf.Infinity, _msk))
                    {
                        if (hit.collider != null)
                        {
                            Teleport(hit);
                        }
                    }
                }
            }
#endif
        }
        else {
            _player = GameObject.FindGameObjectWithTag("Player");
        }
    }
    private void Teleport(RaycastHit hit)
    {
        _gameController.p_Clicks++;
        _player.transform.position = hit.point;
        if (!_gameController.p_PlayerSpawned)
        {
            _gameController.p_PlayerSpawned = true;
        }

        try {
            _gameController._audio.Blink();
        }
        catch {
            print("Blink doesn't work");
        }
    }
}
