﻿using UnityEngine;

public class RoadController : MonoBehaviour
{
	public Transform zeroPoint;
	public Transform endPoint;
    public static RoadController instance;

    private void Awake()
    {
        instance = this;
    }

    private void OnDestroy()
    {
        instance = null;
    }
}
