﻿using UnityEngine;
using System.Collections;
public class Meteor : MonoBehaviour
{
    [SerializeField]
    private float p_speed = 0;
    private GameController _gameController = null;
    private Transform _transform = null;
    private Vector3 _target = Vector3.zero;
    private Vector3 _meteorRespawn = Vector3.zero;
    const float const_maxSpeed = 200;


    private void Start()
    {
        _gameController = GameController.instance;
        p_speed = _gameController.p_BaseSpeed;
        _transform = transform;
        _meteorRespawn = _gameController.meteorPoint.position;
        _target = RoadController.instance.zeroPoint.position + new Vector3(-100, 0, _transform.position.z);
        StartCoroutine(MoveMeteor());
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Trasher")
        {
            if (p_speed < const_maxSpeed)
            {
                p_speed = GameController.instance.p_BaseSpeed;
            }

            else
            {
                p_speed = const_maxSpeed;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
		if (other.tag == "Trasher")
        {
			if (_gameController.p_PlayerDied)
            {
				Destroy (gameObject);
			}
            NewRespawn();
        }

        if (other.tag == "Player")
        {
            other.GetComponent<PlayerCharacter>().UE_PlayerDamaged.Invoke();
            Destroy(gameObject);
        }
	}

    private IEnumerator MoveMeteor()
    {
        while (!_gameController.p_PlayerDied)
        {
            _transform.position += Vector3.left * Time.smoothDeltaTime * p_speed;
            yield return new WaitForFixedUpdate();
        }
    }

    private void NewRespawn()
    {
        _transform.position = new Vector3(_meteorRespawn.x, _meteorRespawn.y, Random.Range(-9, 9));
	}
}
