﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
public class PlayerCharacter : MonoBehaviour {
    
	private bool p_fieldEntered;
	private GameController _gameController;

    public int p_Lives = 10;
    public UnityEvent UE_PlayerDamaged;

    private void Start()
    {
        _gameController = GameController.instance;
        StartCoroutine(IE_TextLives());
        UE_PlayerDamaged.AddListener(PlayerDamaged);
	}

    private void OnTriggerStay(Collider col)
    {
        if (col.tag == "Field")
        {
            gameObject.transform.position += Vector3.left * Time.fixedDeltaTime * _gameController.p_BaseSpeed;
        }
    }
    
    private void OnDestroy()
    {
        UE_PlayerDamaged.RemoveAllListeners();
    }
    #region Custom functions

    private void PlayerDied()
    {
        _gameController.p_PlayerDied = true;
        _gameController.p_GameStarted = false;
        Destroy(gameObject);
		#if UNITY_ANDROID
        ADSManager.instance.CheckAds();
		#endif
    }

    public void PlayerDamaged()
    {
        Handheld.Vibrate();
        p_Lives--;
        if (p_Lives <= 0)
        {
            PlayerDied();
        }
    }

    IEnumerator IE_TextLives()
    {
        while (!_gameController.p_PlayerDied)
        {
            _gameController._uiController._livesText.text = "Lives \n" + p_Lives.ToString();
            yield return null;
        }
    }

    #endregion
}
