﻿using UnityEngine;
public class Statistics : MonoBehaviour
{
    public static Statistics instance;

    private void Awake()
    {
        instance = this;
    }

    public void SaveStats(int l_clicks, int l_death, float l_distance, float l_record)
    {
        PlayerPrefs.SetInt("p_Clicks", l_clicks);
        PlayerPrefs.SetInt("Death", l_death);
        PlayerPrefs.SetFloat("Time", l_distance);
        PlayerPrefs.SetFloat("Record", l_record);
        PlayerPrefs.Save();
    }

    public int Load_Record()
    {
        if (PlayerPrefs.HasKey("Record"))
        {
            int record; record = PlayerPrefs.GetInt("Record");
            return record;
        }
        else
        {
            return 0;
        }
    }
    
    public int Load_clicks()
    {
        if (PlayerPrefs.HasKey("p_Clicks"))
        {
            int clicks; clicks = PlayerPrefs.GetInt("p_Clicks");
            return clicks;
        }
        else
        {
            return 0;
        }
    }

    public int Load_death() {
        if (PlayerPrefs.HasKey("Death"))
        {
            int death; death = PlayerPrefs.GetInt("Death");
            return death;
        }
        else
        {
            return 0;
        }
    }

    public float LoadTimeAlive()
    {
        if (PlayerPrefs.HasKey("Time"))
        {
            float l_distance = PlayerPrefs.GetFloat("Time");
            return l_distance;
        }
        else
        {
            return 0;
        }
    }

    public void ResetStat()
    {
        PlayerPrefs.DeleteAll();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            ResetStat();
        }
    }

    private void OnDestroy()
    {
        instance = null;
    }
}