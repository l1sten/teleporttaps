﻿using UnityEngine;
using UnityEngine.UI;

public class PanelControl : MonoBehaviour
{
    [SerializeField]
    private Statistics _stat;
    [SerializeField]
    private Text _clicks;
    [SerializeField]
    private Text _distance;
    [SerializeField]
    private Text _death;
    [SerializeField]
    private Text _record;

    [SerializeField]
    private GameObject _statPanel;
    private GameObject _currentPanelActive;


    public void ShowStat()
    {
        if (_currentPanelActive == null)
        {
            GameObject _b = Instantiate(_statPanel, transform, false) as GameObject;
            _currentPanelActive = _b;
            GetInfo();
        }
        else
        {
            Destroy(_currentPanelActive);
        }
        
    }

    private void GetInfo()
    {
        Text[] _texts = GetComponentsInChildren<Text>();

        for (int i = 0; i < _texts.Length; i++)
        {
            if (_texts[i].gameObject.name == "p_Clicks")
            {
                _clicks = _texts[i];
                _clicks.text += "\n" + _stat.Load_clicks();
            }
            if (_texts[i].gameObject.name == "Record")
            {
                _record = _texts[i];
                _record.text += "\n" + _stat.Load_Record();
            }
            if (_texts[i].gameObject.name == "Death")
            {
                _death = _texts[i];
                _death.text += "\n" + _stat.Load_death();
            }
            if (_texts[i].gameObject.name == "Distance")
            {
                _distance = _texts[i];
                _distance.text += "\n" + _stat.LoadTimeAlive();
            }
        }

        Button _btn = _currentPanelActive.GetComponentInChildren<Button>();
        _btn.onClick.AddListener(() => DestroyPanel());
    }
    private void DestroyPanel()
    {
        Destroy(_currentPanelActive);
    }
}
