﻿using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public static UIController instance;

	public Button _restartButton;
	public Button _tapButton;
    public Button _menu;

    public Text _restartText;
    public Text _distanceText;
    public Text _centralText;
    public Text _blessText;
    public Text _livesText;
    public Text _recordText;

    public Image _splashImage;
    public Image _tapImage;

    public Button _shareBtn;
    public Button _rateUsBtn;
    
    private Statistics _stat;
    private GameController _gameController;

    public bool p_Texed = false;
    private bool p_oneSave = false;
    

    private void Awake()
    {
        if (instance != null)
        {
            return;
        }

        instance = this;
    }

    private void Start()
    {
        _gameController = GameController.instance;
        _stat = Statistics.instance;

        
		_blessText.text = null;
        _recordText.text = "Record \n" + Statistics.instance.Load_Record();


        AddListeneresOnButtons();
    }

    public void Bless(int l_blessingSwitcher)
    {
        _blessText.enabled = true;
        _blessText.color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f), 1);

        switch (l_blessingSwitcher)
        {
            case 0:
                _blessText.text = "GOD DAMN!";
                break;
            case 1:
                _blessText.text = "YOU DID IT!";
                break;
            case 2:
                _blessText.text = "GOOD JOB!";
                break;
            case 3:
                _blessText.text = "YOU ARE AWESOME!";
                break;
        }
        Invoke("CloseBless", 1f);
        p_Texed = true;
    }

    private void AddListeneresOnButtons()
    {
        _menu.onClick.AddListener(() => SceneControl.instance.ToMenuScene());
        _restartButton.onClick.AddListener(() => _gameController._audio.GetComponent<SceneControl>().RestartGame());
        _rateUsBtn.onClick.AddListener(() => SocialActivityScript.instance.RateUs());
    }

    private void CloseBless()
    {
        p_Texed = false;
        _blessText.text = null;
    }

    public void SetPrepare()
    {
		_gameController.p_GameStarted = true;
		_centralText.enabled = false;
		_tapButton.enabled = false;
		_splashImage.enabled = false;
		_tapImage.enabled = false;

        Instantiate(_gameController._player, new Vector3(50, 1, 0), Quaternion.identity);
		_gameController.p_PlayerSpawned = true;
    }

    public void SetDied(bool l_isDied)
    {
        //Another
        _splashImage.enabled = l_isDied;
        _centralText.enabled = l_isDied;
        _centralText.color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f), 1);
        _centralText.text = "RANGE PASSED: " + _gameController.p_Time.ToString();
        _restartButton.enabled = l_isDied;
        _restartText.enabled = l_isDied;
        _gameController.p_PlayerDied = true;
        _shareBtn.gameObject.SetActive(true);
        _rateUsBtn.gameObject.SetActive(true);

        float l_record;

        if (_stat.Load_Record() < +_gameController.p_Time)
        {
            l_record = _gameController.p_Time;
        } else
        {
            l_record = _stat.Load_Record();
        }
        if (!p_oneSave)
        {
            _stat.SaveStats(_stat.Load_clicks() + _gameController.p_Clicks, 
                _stat.Load_death() + 1,
                _stat.LoadTimeAlive() + _gameController.p_Time, 
                l_record);
			CheckAds ();
            p_oneSave = true;
        }
    }
    public void UIShareImage()
    {
        _gameController._audio.GetComponent<SocialActivityScript>().Sharing_Image();
    }

	private void CheckAds()
	{
		#if UNITY_ANDROID
		ADSManager.instance.CheckAds();
		#endif
	}
}

