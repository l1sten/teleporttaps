﻿using UnityEngine;
using System.Collections;
public class RoadCharacter : MonoBehaviour
{
    private Transform _transform;
    private float p_speed = 50;

	private void Start()
    {
        _transform = transform;
        StartCoroutine(IE_MoveRoads());
	}

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Trasher")
        {
            gameObject.transform.position = RoadController.instance.endPoint.position;
            p_speed += 0.01f;
            GameController.instance.p_Time += 10;
        }
    }

    private IEnumerator IE_MoveRoads()
    {
        while (!GameController.instance.p_PlayerDied)
        {
            if (GameController.instance.p_PlayerSpawned)
            {
                _transform.position += Vector3.left * Time.deltaTime * p_speed;
            }

            yield return new WaitForEndOfFrame();
        }
    }

    
}
