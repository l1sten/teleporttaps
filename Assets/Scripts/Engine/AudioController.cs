﻿using UnityEngine;

public class AudioController : MonoBehaviour
{
    public AudioSource _audioSource;
    public AudioClip[] _clips;
    public AudioSource _music;
    public AudioSource _sfx;

    [SerializeField]
    private AudioClip _blink;
    [SerializeField]
    private AudioClip _crash;
    [SerializeField]
    private AudioClip _died;
    [SerializeField]
    private AudioClip _meteorMove;

	private void Awake ()
    {
		DontDestroyOnLoad (gameObject);
        _clips = Resources.LoadAll<AudioClip>("Media/");

        for (int i = 0; i < _clips.Length; i++)
        {
            if (_clips[i].name == "Blink")
            {
                _blink = _clips[i];
            }
        }

        if (Application.genuine == false)
        {
            Application.CancelQuit();
        }
	}

    public void Blink()
    {
        _sfx.Stop();
        _sfx.clip = _blink;
        _sfx.Play();
    }
    
	
}
