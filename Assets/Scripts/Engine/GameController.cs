﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
public class GameController : MonoBehaviour
{
    public static GameController instance;

	public float p_Time;
    public int p_Clicks = 0;

	public bool p_PlayerDied;
	public bool p_PlayerSpawned;
	public bool p_GameStarted;

	public float p_BaseSpeed = 50f;
    
	public Transform meteorPoint;
	public UIController _uiController;
    public AudioController _audio;
	public GameObject _player;

	public float p_meteorRespawnTimer;
	#if UNITY_ANDROID
	private GooglePlayGamesServicesClass _googlePlayServives;
#endif

    
    
    public delegate void E_ClickAction();
    public static event E_ClickAction e_onclicked;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

        _uiController = gameObject.GetComponent<UIController>();
        _audio = FindObjectOfType<AudioController>();

		#if UNITY_ANDROID
		_googlePlayServives = gameObject.GetComponent<GooglePlayGamesServicesClass>();
		#endif
	}

    private void Start()
    {
        AudioControl();
        StartCoroutine(IE_TextScore());
        StartCoroutine(IE_ClickScreen());
    }

    private void OnDestroy()
    {
        instance = null;
    }

    private void FixedUpdate()
    {
        if (!p_PlayerDied && p_PlayerSpawned)
        { 
            p_Time += Time.deltaTime;
        }

        if (p_Time %50 < 1f)
        {
            p_BaseSpeed++;
            print("tik");
        }

        // похвала игрока
        if (p_Time > 60f && p_Time < 61f && p_Time != 0 && !p_PlayerDied && !_uiController.p_Texed)
        {
            PrisePlayer();
        }
    }

    private void LateUpdate()
    {
        if(p_PlayerDied)
        {
            AnalyticsLog.instance.SendGameOver(p_Time); // analytics
            _uiController.SetDied(true); //игрок умер
			#if UNITY_ANDROID
            _googlePlayServives.OnAddScoreToLeaderBoard(p_Time); //Отправить данные очков на доску лидеров, используя Google Play Games Services (сам скрипт называется GooglePlayGamesServicesClass)
			#endif
		}
    }

    private void AudioControl()
    {
        if (!_audio._music.isPlaying)
        {
            _audio._music.Play();
        }

        _audio._music.volume = 0.75f;
        _audio._sfx.volume = 1f;
    }

    private void PrisePlayer()
    {
        _uiController.Bless(Random.Range(0, 3));
    }

    #region Corotunes
    private IEnumerator IE_ClickScreen()
    {
        while (!p_PlayerDied)
        {
            e_onclicked();
            yield return null;
        }
    }

    private IEnumerator IE_TextScore()
    {
        while (!p_PlayerDied)
        {
            _uiController._distanceText.text = "Time: " + p_Time.ToString("f2") + "sec";
            yield return null;
        }
    }

    #endregion

}
