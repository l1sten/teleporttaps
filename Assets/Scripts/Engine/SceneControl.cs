﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneControl : MonoBehaviour {

    public static SceneControl instance;

    private void Awake()
    {
        if (instance != null)
        {
            return;
        }
        instance = this;
    }

    public void ToBlackScene()
    {
        SceneManager.LoadScene("BLACK");
        Invoke("ToGameScene", 2f);
    }

    public void ToMenuScene()
    {
        SceneManager.LoadScene("Menu");
        Destroy(gameObject);
    }

    public void ToGameScene()
    {
        SceneManager.LoadSceneAsync("Game", LoadSceneMode.Single);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene("Game");
    }
}
