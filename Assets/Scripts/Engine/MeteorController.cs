﻿using UnityEngine;

public class MeteorController : MonoBehaviour
{

	public float p_MeteorTimer;
    public int p_MaxMeteorCount = 3;
    public int p_CurrentMeteorCount = 0;

    public Object meteor;
    private GameController _gameController;

    private void Start()
    {
        _gameController = GameController.instance;
    }

    private void LateUpdate()
    {
        p_MeteorTimer -= Time.deltaTime;
        if (p_MaxMeteorCount > p_CurrentMeteorCount)
        {
            if (p_MeteorTimer <= 0 && GameController.instance.p_GameStarted)
            {
                SpawnMeteor();

                p_MeteorTimer = 1f;
                if (p_MaxMeteorCount < 40)
                {
                    p_MaxMeteorCount += 2;
                }
            }
        }
    }

    public void SpawnMeteor()
    {
		if (p_MaxMeteorCount > p_CurrentMeteorCount)
        {
            GameObject l_delta = Instantiate (meteor, new Vector3(_gameController.meteorPoint.position.x, _gameController.meteorPoint.position.y, Mathf.RoundToInt(_gameController.meteorPoint.position.z + Random.Range(-8, 8))), Quaternion.identity) as GameObject;
            l_delta.name = p_CurrentMeteorCount.ToString();
            p_CurrentMeteorCount++;
			p_MeteorTimer = _gameController.p_meteorRespawnTimer;
		}
	}
}

