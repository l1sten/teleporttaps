﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.SceneManagement;

public class AnalyticsLog : MonoBehaviour
{
    public static AnalyticsLog instance;
    private bool p_firstRuntime = true;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    private void Start()
    {
        StartCoroutine(IE_Analytics());
    }

    IEnumerator IE_Analytics()
    {
        while (gameObject.activeInHierarchy)
        {
            if (p_firstRuntime)
            {
                SendRunGame();
                p_firstRuntime = false;
            }

            if (SceneManager.GetActiveScene().name == "Game")
            {
                SendBeginGame();
            }

            if (SceneManager.GetActiveScene().name == "Menu")
            {
                SendEnterMainMenu();
            }

            yield return null;
        }
    }

    public void SendGameOver(float l_time)
    {
        Analytics.CustomEvent("gameOver", new Dictionary<string, object>
        {
            {"Time", l_time}
        });
    }

    public void SendRunGame()
    {
        Analytics.CustomEvent("runGame");
    }

    public void SendBeginGame()
    {
        Analytics.CustomEvent("beginGame");
    }

    public void SendEnterMainMenu()
    {
        Analytics.CustomEvent("enterMainMenu");
    }
}
